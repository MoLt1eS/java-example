package com.mycompany.app;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        System.out.println( "Hello World!" );

        System.out.println(divideTwoNumbers(5, 0));
    }

    /**
     * Divides two numbers
     * @param a first number
     * @param b
     * @return
     * @throws Exception
     */
    public static double divideTwoNumbers(double a, double b) throws Exception{

        if(b == 0){
            throw new Exception("Divided by zero exception");
        }

        return a/b;
    }
}
